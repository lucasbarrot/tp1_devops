terraform {
  required_providers {
    aws = {
      source = "hashicorp/aws"
      version = "3.11.0"
    }
  }
}

provider "aws" {
  region = "eu-west-1"
}

//key pair
resource "aws_key_pair" "deployer" {
  key_name = "deployer"
  public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQC6X9qTpSUMVOq3GRD6kNFn7b20Ix11l/HjETaknful2q4JfrnSD/th6HJnGq25X7NO/kdSNIDVpMbk1C1egUU26Hu6NfwEK0cfJ0DbwhDo1JnNv/ciq+LJuzoQ2dxO0W4ek05EfYQR0Nv8lHEDZ4NGtJ926NFg+plNFGMQkg1c9W7oBaKq67Idict5zhua0ntj39KHe2jWQOmFmwlDQ9c68lbYpvCr9SKIJy3FT61BJChkOv1NuwSwB5vfXQ/JJhcoPAIyAzyRzj8mHZAXTlob6lFcyea8eIc7GMM7nk6liHxxtFlUY7GLXE4GxYSF+puDdq5ctMALYo36IMKTxaC2x1gZqYfQQI+91LJd7kHQHkw5D2W+7Wl0clTHvbaC2/uFt3seIsRrKg6qg1Ka4kbDqmNB2Nzv9UXdIaDYwzc/VWBMD0HoKjmFeoLlaRtx2/gdyUCm4SCa0YZOvJPdXYgOc2ukYW2fVEfZXHcztXHVCRy1tKlOS8NDURWiiyiFLRk= hugot@hugoT"
}

//vpc
resource "aws_default_vpc" "default" {
  tags = {
    Name = "Default VPC"
  }
}

//aws security
resource "aws_default_security_group" "default" {
  vpc_id = aws_default_vpc.default.id

  ingress {
    protocol  = "tcp"
    from_port = 22
    to_port   = 22
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    protocol  = "tcp"
    from_port = 80
    to_port   = 80
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port = 0
    to_port = 0
    protocol = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

#CONSTRUCT EC2
data "aws_ami" "ubuntu" {
  most_recent = true

  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-focal-20.04-amd64-server-*"]
  }
  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }

  owners = ["099720109477"] # Canonical
}

resource "aws_instance" "web" {
  ami = data.aws_ami.ubuntu.id
  instance_type = "t2.micro"

  key_name = "deployer"

  vpc_security_group_ids = [aws_default_security_group.default.id]

  tags = {
    Name = "srv_1"
  }
}
